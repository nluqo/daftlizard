class StatusIcon{
	constructor(sprite, monster, isBigSprite){
		this.monster = monster;
		this.initSprite(sprite);
	}

	initSprite(sprite){
		this.sprite = new PIXI.Sprite(draw.getIconTexture(sprite));
		this.monster.getContainer().addChild(this.sprite);
	}

	setSprite(sprite){
		this.sprite.texture = draw.getTexture(sprite);
		console.log(sprite);
	}
}

class Status{
	constructor(sprite, monster, duration, bigSpriteIndex){
		this.monster = monster;
		this.spriteIndex = sprite;
		this.spriteIcon = new StatusIcon(sprite, monster);
		if(bigSpriteIndex){
			this.bigSprite = new StatusIcon(bigSpriteIndex, monster, true);
			this.bigSpriteIndex = bigSpriteIndex
		}
		this.duration = duration;
		this.ttl = duration;
		this.level=1;
		this.dispellable=true;
		this.canStack = !!this.constructor.canStack;
	}

	highlight(){
		return `<span class="highlight">(${this.level})</span>`;
	}

	hasMultipleStacks(){
		return this.canStack && this.level > 1;
	}

	hasMultipleDisplayCount(){
		return this.hasMultipleStacks() || (this.displayTtl && this.ttl > 1);
	}

	getIconDisplayCount(){
		if(this.hasMultipleDisplayCount()){
			if(this.hasMultipleStacks()){
				return this.level;
			}else{
				return this.ttl;
			}
		}else{
			return 0;
		}
	}

	setBigSprite(sprite){
		this.bigSprite.setSprite(sprite);
	}

	setIconPosition(x, y){
		this.spriteIcon.sprite.x = x;
		this.spriteIcon.sprite.y = y;
	}

	setDirection(direction){
		this.direction = direction;
		this.setBigSprite(this.bigSpriteIndex + direction);
	}

	getDescription(){
		return this.description;
	}

	statusTick(){

	}

	statusBegin(){

	}

	statusEnd(){

	}

	destroy(){
		this.statusEnd();
		this.monster.getContainer().removeChild(this.spriteIcon.sprite);
		if(this.bigSpriteIndex){
			this.monster.getContainer().removeChild(this.bigSprite.sprite);
		}
		this.monster = null;
	}
}

class StatusShocked extends Status{
	static description = "When damaged, redeal that damage to a nearby enemy in 2 tiles and removed Shocked.";

	constructor(monster){
		super(1, monster, -1);
		this.name = "Shocked";
		this.description = StatusShocked.description;
		this.tint = 0xffff00;
	}
}

class StatusIlluminated extends Status{
	static canStack = true;

	constructor(monster){
		super(5, monster, 30);
		this.name = "Illuminated";
		this.tint = 0xf800ff;
	}

	getDescription(){
		return `Increases spell damage taken by 1 per stack ${this.highlight()}.`;
	}
}

class StatusEclipsed extends Status{
	static canStack = true;

	constructor(monster){
		super(6, monster, 30);
		this.name = "Eclipsed";
		this.tint = 0xf800ff;
	}

	getDescription(){
		return `Reduces damage taken by 1 per stack ${this.highlight()}. Removed upon taking damage.`;
	}
}

class StatusStasis extends Status{

	constructor(monster){
		super(7, monster, 10);
		this.name = "Stasis";
		this.description = "";
		this.dispellable=false;
		this.displayTtl = true;
	}

	getDescription(){
		return `Can't act.  Damage taken reduced by 1
		<span class="highlight">(${map.getNumNonBossEnemies()})</span> for each other enemy.
		When Stasis ends, it breaks all glass items.`;
	}

	statusEnd(){
		if(this.monster.sprite){
			this.monster.sprite.animationSpeed = 0.05;
			this.monster.sprite.tint = 0xFFFFFF;

			if(!this.monster.dead){
				//shake and destroy all keys
				map.getAllTiles().forEach(t=>{
					const item = t.item;
					if(item && item.fragile){
						item.removeFromTile();
						if(item.fragileSpawn){
							t.createItem(item.fragileSpawn);
						}
					}
				});
				game.addScreenshake(20);
				main.playSound("glassBreak");
			}
		}
	}
}

class StatusBerserk extends Status{
	static canStack = true;

	constructor(monster){
		super(8, monster, 25);
		this.name = "Berserk";
        this.description = "Increases damage by 1 per stack.";
		this.tint = 0xFF0000;
	}

	getDescription(){
		return `Increases damage by 1 per stack ${this.highlight()}.`;
	}
}

class StatusTempered extends Status{
	static canStack = true;

	constructor(monster){
		super(15, monster, 7);
		this.name = "Tempered";
		this.tint = 0xff6700;
        this.description = "Reduces damage taken by 1 per stack.";
	}

	getDescription(){
		return `Reduces damage taken by 1 per stack ${this.highlight()}.`;
	}
}

class StatusFireImmunity extends Status{

	constructor(monster){
		super(14, monster, 5);
		this.name = "Fire Immunity";
        this.description = "Take 0 damage from FIRE and BURNING";
		this.tint = 0xff6700;
	}
}

class StatusVigor extends Status{
	static canStack = true;

	constructor(monster){
		super(9, monster, 25);
		this.name = "Vigor";
        this.description = "Doubles healing received.";
		this.tint = 0xFF0000;
	}
}

class StatusScent extends Status{
	static description = "Blood Droplets target this unit.";

	constructor(monster){
		super(2, monster, 25);
		this.name = "Scent";
        this.description = StatusScent.description;
		this.tint = 0xFF0000;
	}
}

class StatusEpiphany extends Status{
	static description = "Your next spell costs 1 less per stack.";
	static canStack = true;
	constructor(monster){
		super(16, monster, -1);
		this.name = "Epiphany";
        this.description = StatusEpiphany.description;
		this.tint = 0xffff00;
	}
}

class StatusEmpowered extends Status{
	static description = "Spell power increases by 1 per stack.";
	static canStack = true;
	constructor(monster){
		super(17, monster, 1);
		this.name = "Empowered";
        this.description = StatusEmpowered.description;
		this.tint = 0x00FFFF;
	}

	getDescription(){
		return `Spell power increases by 1 per stack. ${this.highlight()}.`;
	}
}

class StatusBolstered extends Status{
	static description = "Gain bonus HP which decays 1 per turn.";
	static canStack = true;
	constructor(monster){
		super(18, monster, -1);
		this.name = "Bolstered";
        this.description = StatusBolstered.description;
		this.tint = 0xFF0000;
	}

	statusTick(){
		this.monster.subtractHp(1);
		if(this.level <= 0){
			this.ttl = 0;
		}
	}
}

class StatusBoss extends Status{

	constructor(monster){
		super(10, monster, -1);
		this.name = "Boss";
        this.description = "This monster acts at 2x speed (when active). Drops a Glass Key on death.";
		this.dispellable=false;
	}
}

class StatusStormed extends Status{
	static description = "Emits chain lightning every turn.";

	constructor(monster){
		super(4, monster, main.spellMap["Storm of Vengeance"].duration);
		this.name = "Stormed";
		this.description = StatusStormed.description;
		this.tint = 0xffff00;
	}

	statusTick(){
		const stormBolt = main.spellMap["Storm Bolt"];
		let stormTargetTile = this.monster.tile;
        if(stormTargetTile){
        	spells.effectsProcessor({
        		caster: this.monster,
        		targets: stormTargetTile,
        		effects: stormBolt.effects,
        		spellType: stormBolt.type,
        		spell: stormBolt
        	});  
        }
	}
}



class StatusBurning extends Status{
	static description = "Deal 1 damage per stack.  Loses half stacks each turn.";
	static canStack = true;

	constructor(monster){
		super(3, monster, -1);
		this.name = "Burning";
		this.description = StatusBurning.description;
		this.tint = 0xff6700;
	}

	statusTick(){
		this.monster.damage(this.level, "FIRE");
		if(this.level === 1){
			this.ttl = 0;
		}
		this.level = Math.floor(this.level/2);
	}

	getDescription(monster){
		let description = StatusBurning.description;
		if(monster && monster.maxBurnStacks){
			description +=
				` (Highest Burning: <span style="color:orange;">${monster.maxBurnStacks}</span>)`;
		}
		return description;
	}
}

class StatusAlly extends Status{

	constructor(monster){
		super(13, monster, -1);
		this.name = "Ally";
		this.description = "This monster is friendly.";
		this.dispellable=false;
	}
}

class StatusShield extends Status{
	static description = "Blocks damage from one direction this turn. Benefits from spell power.";
	static canStack = true;

	constructor(monster){
		super(12,monster, 1, 208);
		this.name = "Shield";
		this.direction = 0;
		this.description = StatusShield.description;
		this.tint = 0xbbf5ff;
	}
}





