map = {
	tiles: [],
	monsters: [],
	// typically, you want to use this when accessing monsters
	// "dead" monsters are only kept around temporarily so we don't disturb the loop if we ever iterate it
	// and so we can animate them
	livingMonsters: ()=>map.monsters.filter(m=>!m.dead),
	generate: function(){
		util.tryTo('generate map', function(){
			tileContainer.removeChildren();
			game.cleanUpDrawables();

			game.consoleLog("generate map attempt")

	        const noDisconnectedIslands = map.generateTiles() ==
	        	map.randomPassableTile().getConnectedTiles(t=>t.walkable()).length;

	        const exitNotBlocked = map.stairs.getAdjacentNeighbors().filter(t=>t.passable).length > 0;

	        return noDisconnectedIslands && exitNotBlocked;

	    });


		map.generateMonsters();
		map.generateItems();
	},
	generateItems: function(){
		const items = [
			ItemTalisman,
			ItemTalisman,
			ItemBrokenHourglass
		];

		if(util.random()<.7){
			//binned until later patches (currently identical to health gear and too powerful)
			//items.push(ItemHeart);
		}

		if(game.levelIndex > 0){
			items.push(ItemPotion);
		}
		items.push(ItemPotion);

		if(util.random()<.5 && game.levelIndex == 0){
			items.push(ItemBrokenHourglass);
		}

		//increase chance per level
		if(util.random() < (game.levelIndex / 10)){
			items.push(ItemTalisman);
		}

		// 3, 6, 9
		if(game.levelIndex % 3 == 2){
			items.push(ItemHourglass);
		}
		
		if(game.levelIndex == 0){
			//items.push(ItemHourglass);
		}


		// only 1 key on first 3 levels
		items.push(ItemGlassKey);
		items.push(ItemChest);
		if(game.levelIndex > 2){
			items.push(ItemGlassKey);
			items.push(ItemChest);
		}

		items.forEach(itemClass=>{
			map.randomPassableTile().createItem(itemClass);
		});
	},
	// returns the number of walkable tiles (floor, not stairs or walls)
	// this is so that when checking for disconnected islands, a stair is not counted
	// (an open stair blocks movement sort of )
	generateTiles: function(){
		map.cleanup();

		let fillPercent = util.random()*.25+.05;
		const r = util.random();
		if(r<.25){
			fillPercent = 0.05;
		}else if(r<.4){
			fillPercent = 0.3;
		}

		fillPercent = .32;

	    let walkableTiles=0;
	    map.tiles = [];
		for(i=0;i<mapSize;i++){
			map.tiles[i] = [];
			for(j=0;j<mapSize;j++){
				if(util.random()<fillPercent || !map.inBounds(i,j,1)){
					map.tiles[i][j] = new Wall(i,j);
				}else{
					const floorSpriteLocation = ((i+j)%2)*16+2;
					map.tiles[i][j] = new Floor(i,j);
					walkableTiles++;
				}
			}
		}

	    map.stairs = map.randomPassableTile().replace(Stairs);
	    walkableTiles--;

	    return walkableTiles;
	},
	cleanup: function(){
		map.getAllTiles().forEach(t=>{
			t.cleanupSprite();
			if(t.mana){
				t.mana.cleanupSprite();
			}
			if(t.item){
				t.item.cleanupSprite();
			}

			map.livingMonsters().forEach(m=>m.cleanupSprite());

			map.monsters = [];
		});
	},
	getAllTilesWithMonsters: function(specificMonsterName){
		if(specificMonsterName){
			// game.consoleLog("specific name = " + specificMonsterName);
			// game.consoleLog(map.getAllTiles().filter(t=>t.monster && t.monster.getName()==specificMonsterName));
			return map.getAllTiles().filter(t=>t.monster && t.monster.getName()==specificMonsterName);
		}
		return map.getAllTiles().filter(t=>t.monster);
	},
	getAllTilesWithMana: function(){
		return map.getAllTiles().filter(t=>t.mana);
	},
	getAllTiles: function(){
		const tiles = [];
		if(map.tiles[0]){
			for(i=0;i<mapSize;i++){
				for(j=0;j<mapSize;j++){
					tiles.push(map.tiles[i][j]);
				}
			}
		}
		return tiles;
	},
	getClosestEmptyTile:function(target){//target is a tile
		return map.getAllTiles()
                        .filter(y=>y.passable && !y.monster && !y.mana && !y.item)
                        .sort((a,b)=>{
                            return target.distance(a)-target.distance(b);
                        })[0];
	},
	pickAndCreateMonster: function(floorStrength){
		const monsterType = map.pickMonsterType(floorStrength);
		const monster = new Monster(monsterType);	
		map.monsters.push(monster);
		return monster;
	},
	getBaseNumMonsters: function(index){
		let floorNum = index+1;
		switch(floorNum){
			case 1:
				return 3;
			case 2:
			case 3:
				return 4;
			case 4:
			case 5:
			case 6:
				return 5;
			case 7:
			case 8:
				return 6;
			case 9:
			case 10:
				return 7;
		}

		return 0;
	},
	generateMonsters: function(){
		map.monsters = [];

		let numMonsters = map.getBaseNumMonsters(game.levelIndex);

		const maxFloorStrength = 10;
		const floorStrength = Math.min(maxFloorStrength, game.levelIndex + 1);
		for(let i=0;i<numMonsters;i++){
			map.pickAndCreateMonster(floorStrength);
		}

		//windshield mobs
		for(let i=0;i<floorStrength-2;i++){
			map.pickAndCreateMonster(util.randomRange(1,floorStrength-2));
		}

		// if(map.livingMonsters()){
		// 	const boss = map.pickAndCreateMonster(floorStrength+2);
		// 	boss.addStatus(StatusStasis);
		// 	boss.addStatus(StatusBoss);
		// 	boss.sprite.animationSpeed = 0;
		// }
	},
	pickMonsterType: function(monsterLevel){
		return util.pickRandom(main.monsterTypes.filter(m=>{
			return m.level==monsterLevel
		}));
	},
	randomPassableTile: function(){
	    let tile;
	    util.tryTo('get random passable tile', function(){
	        let x = util.randomRange(0,mapSize-1);
	        let y = util.randomRange(0,mapSize-1);
	        tile = map.getTile(x, y);
	        return tile.passable && !tile.monster && !tile.mana && !tile.item && !tile.preventStuff;
	    });
	    return tile;
	},
	inBounds: function(x, y, borderSize){
		if(!borderSize) borderSize = 0;
	    return x>=borderSize && y>=borderSize && x<=mapSize-1-borderSize && y<=mapSize-1-borderSize;
	},
	getTile: function(i, j){
		if(!map.inBounds(i,j)){
			if(!map.dummyWall){
				map.dummyWall = new Wall(-1000,-1000);
			}
			return map.dummyWall;
		}else{
			return map.tiles[i][j];
		}
	},
	//returns true if it was updated
	selectTile: function(tileX, tileY, skipIntents){
		const previouslySelectedTile = map.selectedTile;
		if(game.state == "running"){
			map.clearSelectTile();
	    	if(map.inBounds(tileX,tileY)){
	    		map.selector.x = tileX;
	    		map.selector.y = tileY;
	    		map.selector.updateDrawPosition();

	    		map.selectedTile = map.getTile(tileX, tileY);

	    		let highlightType = "CURSOR";
	    		if(cards.state == "TARGET"){
	    			if(!spells.isValidTarget()){
	    				highlightType = "ERROR";
	    			}
	    		}else if(cards.state=="POSITION"){
	    			if(!player.canMove(map.selectedTile)){
	    				highlightType = "ERROR";
	    			}
	    		}

	    		map.selectedTile.setHighlight(highlightType);

   				main.updateHoverDescription();

   				if(!skipIntents){
   					game.drawIntents(map.selectedTile.monster, /* fromSelectTile=*/ true);
   				}
	    	}
	    }

	    return previouslySelectedTile != map.selectedTile;
    },
    clearSelectTile: function(){
    	if(map.selectedTile){
    		map.selectedTile.unsetHighlight(["CURSOR","ERROR"]);
    		map.selectedTile = null;
    		if(cards.state == "PROCESSING"){
    			debugger;
    		}

    		document.querySelector("#sidebar-hover-info").innerHTML = "";
    	}
    },
    getNumNonBossEnemies: function(){
    	return map.monsters.filter(m=>!m.ally && !m.hasStatus(StatusStasis)).length;
    },
    getAllItems: function(){
    	return map.getAllTiles().map(t=>t.item).filter(item=>item);
    }
}